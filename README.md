Opis

Zadatak ovog projekta bio je napraviti klasu koja će za datoteku BitcoinHeistData.csv omogućiti:

1. Pretraživanje zapisa po ključu ili vrijednosti, pretraživanje n zapisa po
 ključu ili vrijednosti
2. Brisanje zapisa po ključu ili vrijednosti, brisanje n zapisa po ključu ili
 vrijednosti
3. Dohvaćanje najvećih i najmanjih vrijednosti, dohvaćanje n najvećih i
 najmanjih zapisa po vrijednosti
4. Dodavanje zapisa po ključu ili vrijednosti, dodavanje n zapisa po ključu
 ili vrijednosti.

 U ovom projektu koristio sam sljedeće strukture podataka:

std::vector<std::unordered_map<int, std::string>>: Ova struktura podataka koristi se za pohranu podataka o Bitcoin transakcijama u obliku vektora neuređenih mapa. Svaka neuređena mapa predstavlja jedan redak podataka, gdje je ključ cijeli broj koji predstavlja indeks stupca, a vrijednost je string koji sadrži vrijednost podataka.

std::unordered_map<int, std::string>: Ova struktura podataka koristi se za predstavljanje pojedinačnog retka podataka. Ključevi su  brojevi koji predstavljaju indekse stupaca, dok su vrijednosti stringovi koji sadrže stvarne vrijednosti podataka.

Funkcije koje sam koristio za ovaj projekt su:

loadData - za učitavanje podataka iz csv datoteke

searchData - za pretraživanje po ključu i vrijednosti

deleteData - za brisanje po ključu i vrijednosti

getHighestValue - za dohvat najveće ili n najvećih vrijednosti

getLowestValue -  za dohvat najmanje ili n najmanjih vrijednosti

addRecord - za dodavanje novih zapisa

Rezultati mjerenja su sljedeći:

Vrijeme potrebno za ucitavanje podataka: 16224 ms

Vrijeme potrebno za pretrazivanje podataka: 3 ms

Vrijeme potrebno za brisanje podataka: 528 ms

Vrijeme potrebno za dohvat najvece vrijednosti: 9183 ms

Vrijeme potrebno za dohvat najmanje vrijednosti: 9174 ms

Vrijeme potrebno za dodavanje zapisa: 120 ms


Vrijeme potrebno za pretrazivanje podataka(n=5): 16 ms

Vrijeme potrebno za brisanje podataka(n=5): 2660 ms

Vrijeme potrebno za dohvat najvece vrijednosti(n=5): 9779 ms

Vrijeme potrebno za dohvat najmanje vrijednosti(n=5): 12795 ms
