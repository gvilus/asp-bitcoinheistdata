#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include <unordered_map>
#include <chrono>

class BitcoinDataHandler
{
public:
    BitcoinDataHandler()
    {
        Types();
    }

    void loadData(const std::string &file_path)
    {
        auto start = std::chrono::steady_clock::now();

        std::ifstream file(file_path);
        std::string line, cell;

        if (!file.is_open())
        {
            std::cerr << "Error opening file: " << file_path << std::endl;
        }

        std::getline(file, line);

        while (std::getline(file, line))
        {
            std::istringstream linestream(line);
            std::unordered_map<int, std::string> row;
            int columnIndex = 0;

            while (std::getline(linestream, cell, ','))
            {
                row[columnIndex++] = cell;
            }

            data.push_back(row);
        }

        file.close();

        auto end = std::chrono::steady_clock::now();
        std::cout << "Vrijeme potrebno za ucitavanje podataka: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;
    }

    void searchData(int key, const std::string &value, int n)
    {
        auto start = std::chrono::steady_clock::now();

        int totalFound = 0;
        std::string type = dataTypes[key];
        double numericValue = convertValue(value, type);

        for (const auto &row : data)
        {
            auto it = row.find(key);
            if (it != row.end())
            {
                double rowValue = convertValue(it->second, type);
                if ((type == "string" && it->second == value) || (type != "string" && rowValue == numericValue))
                {
                    for (int i = 0; i < 10; ++i)
                    {
                        auto innerIt = row.find(i);
                        if (innerIt != row.end())
                        {
                            std::cout << innerIt->first << ": " << innerIt->second << ", ";
                        }
                    }
                    std::cout << std::endl;
                    totalFound++;

                    if (n > 0 && totalFound >= n)
                    {
                        break;
                    }
                }
            }
        }
        if (totalFound == 0)
        {
            std::cout << "Nije pronaden niti jedan zapis." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::cout << "Vrijeme potrebno za pretrazivanje podataka: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;
    }

    void deleteData(int key, const std::string &value, int n)
    {
        auto start = std::chrono::steady_clock::now();

        int totalDeleted = 0;
        std::string type = dataTypes[key];
        double numericValue = convertValue(value, type);

        for (auto it = data.begin(); it != data.end();)
        {
            if (it->find(key) != it->end())
            {
                double rowValue = convertValue(it->at(key), type);
                if ((type == "string" && it->at(key) == value) || (type != "string" && rowValue == numericValue))
                {
                    it = data.erase(it);
                    totalDeleted++;

                    if (n > 0 && totalDeleted >= n)
                    {
                        break;
                    }
                }
                else
                {
                    ++it;
                }
            }
            else
            {
                ++it;
            }
        }

        if (totalDeleted == 0)
        {
            std::cout << "Nije pronaden nijedan takav zapis." << std::endl;
        }
        else
        {
            std::cout << "Obrisano " << totalDeleted << " zapisa." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::cout << "Vrijeme potrebno za brisanje podataka: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;
    }

    void getHighestValue(int key, int n)
    {
        auto start = std::chrono::steady_clock::now();

        if (data.empty())
        {
            std::cout << "Nema podataka." << std::endl;
        }

        std::string type = dataTypes[key];
        std::multimap<double, std::unordered_map<int, std::string>, std::greater<>> sorted;

        for (const auto &row : data)
        {
            auto it = row.find(key);
            if (it != row.end())
            {
                double vrijednost = convertValue(it->second, type);
                sorted.insert({vrijednost, row});
            }
        }

        int totalDisplayed = 0;
        for (const auto &item : sorted)
        {
            if (n > 0 && totalDisplayed >= n)
            {
                break;
            }
            for (int i = 0; i < 10; ++i)
            {
                auto innerIt = item.second.find(i);
                if (innerIt != item.second.end())
                {
                    std::cout << innerIt->first << ": " << innerIt->second << ", ";
                }
            }
            std::cout << std::endl;
            totalDisplayed++;
        }

        if (totalDisplayed == 0)
        {
            std::cout << "Nije pronaden niti jedan zapis." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::cout << "Vrijeme potrebno za dohvat najvece vrijednosti: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;
    }

    void getLowestValue(int key, int n)
    {
        auto start = std::chrono::steady_clock::now();

        if (data.empty())
        {
            std::cout << "Nema podataka." << std::endl;
            return;
        }

        std::string type = dataTypes[key];
        std::multimap<double, std::unordered_map<int, std::string>> sorted;

        for (const auto &row : data)
        {
            auto it = row.find(key);
            if (it != row.end())
            {
                double vrijednost = convertValue(it->second, type);
                sorted.insert({vrijednost, row});
            }
        }

        int totalDisplayed = 0;
        for (const auto &item : sorted)
        {
            if (n > 0 && totalDisplayed >= n)
            {
                break;
            }
            for (int i = 0; i < 10; ++i)
            {
                auto innerIt = item.second.find(i);
                if (innerIt != item.second.end())
                {
                    std::cout << innerIt->first << ": " << innerIt->second << ", ";
                }
            }
            std::cout << std::endl;
            totalDisplayed++;
        }

        if (totalDisplayed == 0)
        {
            std::cout << "Nije pronaden niti jedan zapis." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::cout << "Vrijeme potrebno za dohvat najmanje vrijednosti: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;
    }

public:
    void addRecord(const std::vector<std::unordered_map<int, std::string>> &newRecords) {
        auto start = std::chrono::steady_clock::now();

        for (const auto &zapis : newRecords) {
            data.push_back(zapis);
        }

        auto end = std::chrono::steady_clock::now();
        std::cout << "Dodano " << newRecords.size() << " novih zapisa." << std::endl;
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme potrebno za dodavanje zapisa: " << vrijeme.count() << " ms" << std::endl;
    }

private:
    std::vector<std::unordered_map<int, std::string>> data;
    std::unordered_map<int, std::string> dataTypes;

    void Types()
    {
        dataTypes = {
                {0, "string"},
                {1, "int"},
                {2, "int"},
                {3, "int"},
                {4, "float"},
                {5, "int"},
                {6, "int"},
                {7, "int"},
                {8, "float"},
                {9, "string"}};
    }

    static double convertValue(const std::string &value, const std::string &type);
};

double BitcoinDataHandler::convertValue(const std::string &value, const std::string &type)
{
    try
    {
        if (type == "int")
        {
            return std::stoi(value);
        }
        else if (type == "float")
        {
            return std::stof(value);
        }
        else
        {
            return 0;
        }
    }
    catch (const std::invalid_argument &e)
    {
        std::cerr << "Dogodila se greska: " << e.what() << std::endl;
        return 0;
    }
}

int main()
{
    BitcoinDataHandler BitcoinDataHandler;

    BitcoinDataHandler.loadData("BitcoinHeistData.csv");

    std::cout << "\nPretrazivanje zapisa u stupcu day s vrijednoscu 132:" << std::endl;
    BitcoinDataHandler.searchData(2, "132", 1);

    std::cout << "\nBrisanje zapisa u stupcu length s vrijednoscu 72:" << std::endl;
    BitcoinDataHandler.deleteData(3, "72", 1);

    std::cout << "\nDohvat zapisa s najvecom vrijednoscu u stupcu day:" << std::endl;
    BitcoinDataHandler.getHighestValue(2, 1);

    std::cout << "\nDohvat zapisa s najmanjom vrijednoscu u stupcu length:" << std::endl;
    BitcoinDataHandler.getLowestValue(3, 1);

    std::vector<std::unordered_map<int, std::string>> newRecords;
    std::unordered_map<int, std::string> newRecord = {
            {0, "ldfgjdflkgdfl"},
            {1, "2015"},
            {2, "99"},
            {3, "77"},
            {4, "0.0001"},
            {5, "1"},
            {6, "0"},
            {7, "2"},
            {8, "1e+09"},
            {9, "princetonPrince"}
    };
    newRecords.push_back(newRecord);
    BitcoinDataHandler.addRecord(newRecords);

    std::cout << "\nPretrazivanje 5 zapisa u stupcu day s vrijednoscu 3:" << std::endl;
    BitcoinDataHandler.searchData(2, "3", 5);

    std::cout << "\nBrisanje 5 zapisa u stupcu length s vrijednoscu 4:" << std::endl;
    BitcoinDataHandler.deleteData(3, "4", 5);

    std::cout << "\nDohvat 5 zapisa s najvecom vrijednoscu u stupcu length:" << std::endl;
    BitcoinDataHandler.getHighestValue(3, 5);

    std::cout << "\nDohvat 5 zapisa s najmanjom vrijednoscu u stupcu income:" << std::endl;
    BitcoinDataHandler.getLowestValue(8, 5);

    return 0;
}
